# Standard
import re
import os
import zipfile
from subprocess import Popen

# Vendor
import numpy as np
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plot


class Runner(object):
    """ Run a list of configuration of DENN """

    def __init__(self, configuration_path, outputs_path, email, bin, gd_version):
        super(Runner, self).__init__()

        # Get output path
        self.configuration_path = configuration_path
        outputs_path = os.path.abspath(outputs_path)
        if not os.path.exists(outputs_path):
            os.mkdir(outputs_path, 0o755)

        # Create output path for the current task
        task_name = "-".join(os.path.splitext(os.path.basename(self.configuration_path))[0].split("-")[1:])
        task_regex = re.compile("^%s.+" % task_name)
        dir_with_same_prefix = len([d for d in os.listdir(outputs_path)
                                    if "%s/%s" % (outputs_path, os.path.isdir(d)) and len(task_regex.findall(d)) == 1])
        self.task_output_path = "%s/%s-%d" % (outputs_path, task_name, dir_with_same_prefix + 1)
        if not os.path.exists(self.task_output_path):
            os.mkdir(self.task_output_path, 0o755)

        self.email = email
        self.bin = bin
        self.gd_version = gd_version

    def execute(self):
        """ Executes the configuration in the indicated file from CLI. """
        with open(self.configuration_path) as f:
            content = f.readlines()

        path_regex = \
            re.compile(".+-em +([a-zA-Z_\-]+).+-m +([a-zA-Z_]+).+-(co|cr) +([a-zA-Z]+).+-rof.+/([a-zA-Z0-9-_.]+)") \
                if not self.gd_version else \
                    re.compile(".+-rof.+/([a-zA-Z0-9-_.]+)")

        configurations = filter(lambda s: len(s) > 0, [configuration.strip().replace("{abs_path}", self.task_output_path) for configuration in content if not configuration[0] == "#"])
        for idx, c in enumerate(configurations):
            print("Executing %d" % idx)
            Popen("%s %s" % (self.bin, c), shell=True).wait()
            path_regex_groups = path_regex.match(c)
            if not self.gd_version:
                self.create_flow_plot(path_regex_groups.group(5),
                                      path_regex_groups.group(1),
                                      path_regex_groups.group(2),
                                      path_regex_groups.group(4))
            else:
                self.create_flow_plot(path_regex_groups.group(1))

        self.notify(self.create_zip())

    def notify(self, zip_filename: str) -> None:
        """ Notify to the email that the process is ended. """
        import smtplib
        from email.mime.text import MIMEText
        from email.mime.base import MIMEBase
        from email.mime.multipart import MIMEMultipart
        from email import encoders

        msg = MIMEMultipart()
        msg["Subject"] = "Hey, look at Unipg Machine Learning Beast!"
        msg["From"] = "ml.unipg.dev@unipg.it"
        msg["To"] = self.email

        body_part = MIMEText("Computation completed! :D", 'plain')
        msg.attach(body_part)

        with open("%s/%s" % (self.task_output_path, zip_filename), "rb") as f:
            mbase = MIMEBase("application", "zip")
            mbase.set_payload(f.read())
            encoders.encode_base64(mbase)
            mbase.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(zip_filename))
            msg.attach(mbase)

        s = smtplib.SMTP("smtp.gmail.com:587")
        s.ehlo()
        s.starttls()
        s.login("belli.valerio@gmail.com", "daqpxbhhxotkjzdy")
        s.send_message(msg)
        s.quit()

    def create_flow_plot(self, position: str, evolution_method: str = None, mutation: str = None, crossover: str = None) -> None:
        """ Create a plot starting from the execution data of a test """
        def get_mutation_parts(mutation_method: str) -> tuple:
            parts = mutation_method.split("/")
            return (parts[0], parts[1] if len(parts) > 1 else 1)
        fig = plot.figure()
        plot_data = []
        line_regex = re.compile("\|-.(\d+).+ACC_VAL:([0-9.]+).+TIME:([0-9.]+)")
        last_idx = -1
        with open("%s/%s" % (self.task_output_path, position)) as f:
            for plot_step in f.readlines():
                res = line_regex.match(plot_step.strip())
                if res is not None:
                    if last_idx < int(res.group(1)) or last_idx is None:
                        last_idx = int(res.group(1))
                        plot_data.append([float(res.group(2)), float(res.group(3))])
        plot_data = np.stack(plot_data)
        ax = fig.add_subplot(111)

        ax.plot(plot_data[:, 1], plot_data[:, 0], "k", linewidth=0.6)
        if not self.gd_version:
            parts = get_mutation_parts(mutation)
            ax.set_title("%s/%s/%d/%s" % (evolution_method, parts[0], parts[1], crossover))
        ax.set_xlabel("Time (sec)")
        ax.set_ylabel("Cost")
        fig.savefig("%s/%s" % (self.task_output_path, os.path.splitext(os.path.basename(position))[0]))

    def create_zip(self) -> str:
        def zipdir(path: str, ziph: zipfile.ZipFile) -> None:
            # ziph is zipfile handle
            for root, dirs, files in os.walk(path):
                for file in files:
                    if os.path.basename(path) != os.path.splitext(file)[0] and \
                        os.path.splitext(file)[1] != "json":
                        ziph.write("%s/%s" % (self.task_output_path, file), file)
        zip_filename = '%s.zip' % os.path.basename(self.task_output_path)
        zipf = zipfile.ZipFile("%s/%s" % (self.task_output_path, zip_filename), 'w', zipfile.ZIP_STORED)
        zipdir(self.task_output_path, zipf)
        return zip_filename