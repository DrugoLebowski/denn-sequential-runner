# Standard
from argparse import ArgumentParser

# Project
from Runner import Runner

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("configuration_path", type=str,
                        help="The path where resides the configuration file.")
    parser.add_argument("outputs_path", type=str,
                        help="The path where reside the output files.")
    parser.add_argument("--bin", "-b", type=str, dest="bin", required=True,
                        help="The path where resides the executable.")
    parser.add_argument("--email", "-e", type=str, dest="email", default="belli.valerio@gmail.com",
                        help="The email where the notification will be sent.")
    parser.add_argument("--gd_version", "-gdv", type=bool, dest="gd_version", default=False,
                        help="Notify whether the version to execute is for DENN or ADAM.")

    args = parser.parse_args()

    Runner(
        configuration_path=args.configuration_path,
        outputs_path=args.outputs_path,
        email=args.email,
        bin=args.bin,
        gd_version=args.gd_version
    ).execute()
